﻿using System;
using UnityEngine.Perception.Randomization.Parameters;
using UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers.Tags;

namespace UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers
{
    /// <summary>
    /// Randomizes the material texture of objects tagged with a TextureRandomizerTag
    /// </summary>
    [Serializable]
    [AddRandomizerMenu("Perception/SmoothnessRandomizer")]
    public class SmoothnessRandomizer : Randomizer
    {
        public FloatParameter smoothnessParameter;
        protected override void OnIterationStart()
        {
            var tags = tagManager.Query<SmoothnessRandomizerTag>();
            foreach (var tag in tags)
            {
                var renderer = tag.GetComponent<Renderer>();
                renderer.material.SetFloat("_Smoothness", smoothnessParameter.Sample());
            }
        }
    }
}
