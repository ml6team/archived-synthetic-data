﻿using UnityEngine;
using UnityEngine.Perception.Randomization.Randomizers;

[AddComponentMenu("Perception/RandomizerTags/BackgroundScaleRandomizerTag")]
public class BackgroundScaleRandomizerTag : RandomizerTag
{
}