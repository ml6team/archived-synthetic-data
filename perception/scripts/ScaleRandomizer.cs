using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Perception.Randomization.Parameters;
using UnityEngine.Perception.Randomization.Randomizers;
using UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers.Tags;
using UnityEngine.Perception.Randomization.Samplers;

[Serializable]
[AddRandomizerMenu("Perception/Scale Randomizer")]
public class ScaleRandomizer : Randomizer
{
    public FloatParameter scaleParameter;

    protected override void OnIterationStart()
    {
        var tags = tagManager.Query<ScaleRandomizerTag>();

        foreach (var tag in tags)
        {   
            var base_scale = 100;
            var scale = scaleParameter.Sample();
            tag.transform.localScale = new Vector3(scale*base_scale, scale*base_scale,scale*base_scale);
        }
    }
}