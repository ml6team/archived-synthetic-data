﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Perception.Randomization.Parameters;
using UnityEngine.Perception.Randomization.Randomizers;
using UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers.Tags;
using UnityEngine.Perception.Randomization.Samplers;

[Serializable]
[AddRandomizerMenu("Perception/Background Scale Randomizer")]
public class BackgroundScaleRandomizer : Randomizer
{
    public FloatParameter scaleParameter;

    protected override void OnIterationStart()
    {
        var tags = tagManager.Query<BackgroundScaleRandomizerTag>();

        foreach (var tag in tags)
        {   
            var base_scale = 10;
            var scale = scaleParameter.Sample();
            tag.transform.localScale = new Vector3(scale*base_scale, scale*base_scale,scale*base_scale);
        }
    }
}