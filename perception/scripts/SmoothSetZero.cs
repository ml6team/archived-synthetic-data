﻿using System;
using UnityEngine.Perception.Randomization.Parameters;
using UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers.Tags;

namespace UnityEngine.Perception.Randomization.Randomizers.SampleRandomizers
{
    /// <summary>
    /// Randomizes the material texture of objects tagged with a TextureRandomizerTag
    /// </summary>
    [Serializable]
    [AddRandomizerMenu("Perception/Smoothness set to zero")]
    public class SmoothSetZero : Randomizer
    {

        protected override void OnIterationStart()
        {
            var tags = tagManager.Query<SmoothSetZeroTag>();
            foreach (var tag in tags)
            {
                var renderer = tag.GetComponent<Renderer>();
                renderer.material.SetFloat("_Smoothness", 0);
            }
        }
    }
}
