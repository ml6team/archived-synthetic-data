# Using Unity Perception to create synthetic data to improve object detection models

Based on this [blog](https://blog.unity.com/technology/training-a-performant-object-detection-ml-model-on-synthetic-data-using-unity-perception) from unity.

Obtaining a large real world dataset for a good object detection model can be a challenge, requiring a large amount of labeled pictures of objects in front of diverse background and from multiple different angles.

If it is possible to 3D scan your objects, you can greatly improve your results by adding synthetic data to your dataset. Using Unity Perception, you can easily create large amounts of labeled synthetic data.

## Requirements

* The latest version of Unity Editor 2020.2.x. from [here](https://unity3d.com/get-unity/download/archive) or from [Unity Hub](https://unity3d.com/get-unity/download) 

* Phone with Lidar camera and a [3D scanning app](https://scaniverse.com/) or other 3D scanning tools.

* [Blender](https://www.blender.org/) or other 3D editing software.

* [Tensorflow](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html) installation with gpu support (recommended).

* [Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection) from tensorflow.

Several python packages:

* LabelImg for manually labeling your images
* EasyDict
* DatasetInsights
* pascal_voc


## Setting up Unity perception with your 3D scans

Open [this github tutorial](https://github.com/Unity-Technologies/com.unity.perception/blob/main/com.unity.perception/Documentation~/Tutorial/Phase1.md) and follow at least the first 3 steps. This will tell you how to install Unity Perception and how to create the scene.


After completing the first 3 steps, also complete the 4th step until they ask you to open the tutorial prefabs (until label configurations are ready). Now you need to add your own object files to the project.
If you want to test this out using the tutorial objects you can continue step 4 of the tutorial, if not create a new folder under samples/Perception/Object Prefabs for your own objects prefabs.

Now you will need to make your object scans.
For 3D scanning your objects, it is recommended to have good lighting on all angles of the object. 
If You cannot capture all angles of the object with the scanner you can edit your objects using blender afterwards. 


If your object has a flat surface, you can, for example, put the object on a pedestal with good lighting, scan in your object and afterwards take a seperate picture of the bottom surface to add it in blender.  
If your object is very lightweight you might be able to let it hang from a thread; this way you can scan the bottom as well, thin segments such as threads will not be captured by most scanning apps anyway.  
Crop your scan so only the complete object remains.

Export your objects as .obj or .fbx files and open them in blender. If not already the case, center your object around its center. Once they are ready you can open your object folder in your unity perception project and drag the objects in the scene. 

![alt text](demo_images/demo_1.PNG)  

If the texture did not correctly transfer, copy the texture file from blender to your unity project and manually add a material (right click in folder --> create --> material), then drag your texture image to the gray square next to "Base Map" in the inspector panel of the material.
Then add the material to your object in the object inspector panel. 

![alt text](demo_images/demo_2.PNG)  

Make sure all your objects have the same base scale in unity, if not set a new base scale in blender.
Once you objects are correctly displayed in your unity scene, unpack them and drag the mesh from your scene into your Object Prefabs folder. If a window pops up asking whether to make an original prefab or a variant, make your objects original prefabs.


The following steps are similar to step 4 of the tutorial linked above, see there for more info.
Select all your prefab files in your Object Prefabs folder and in the inspector tab, select a labeling scheme. You can use automatic labeling with the asset names as your labels (make sure to name your prefabs correctly). However if you use multiple meshes for the same object (which can be useful as it can provide more diverse images), you can instead manually label your assets. 

![alt text](demo_images/demo_3.PNG)  

Then click Add Automatic Labels of All Selected Assets to Config
And in the window that pops up, click "add all labels" for both configurations. 

## Generating the images

Now create a new GameObject by right clicking your hierarchy tab and clicking "create empty", rename this to "Simulation".
in the inspector view for this object, add a fixed length scenario component. Here you can choose how many images you want to create under "Total Iterations".

![alt text](demo_images/demo_4.PNG)  

On this github page under Perception/scripts, there are some scripts you can use to change and improve (depending on your goals) your simulation. 

The simulation used in the tutorial uses a large amount of random background objects with random textures in order to create really diverse backgrounds. However through experimentation I have found that using real background images with some random objects in the foreground aside from the training data objects works better. This is likely because these random background object backgrounds do not represent reality well. 

![tutorial](demo_images/rgb_2.png)  

However if you still want to use the large amount of random background objects as background, you should continue the tutorial linked above from step 5 onwards.
For the solution presented here, import the scripts and tags from this page into the assets/scripts folder in your perception project.

On the inspector tab of your simulation, click add randomizer and add the following randomizers:

  * BackgroundObjectPlacementRandomizer
  * SmoothSetZero
  * ForegroundObjectPlacementRandomizer
  * ForegroundRandomObjectPlacementRandomizer
  * TextureRandomizer
  * HueOffsetRandomizer
  * RotationRandomizer
  * MyLightRandomizer
  * ScaleRandomizer
  * BackgroundScaleRandomizer
  * SmoothnessRandomizer

Ordering is important in some cases! For example rotation and textures can only be changed after objects have been placed!
Now each randomizer will be explained shortly:

### BackgroundObjectPlacementRandomizer

![alt text](demo_images/demo_5.PNG) 

For this randomizer, import the cube object from perception/objects/scene_wall into your project. In the randomizer in the inspector tab, click add folder and select the folder with this object, give the object a probability of 1 (click off uniform probability to change probability).
Choose The placement area 10 - 10 and separation distance 6 with only 1 layer. This way multiple images will be generated so there will always be a background but only 1 should be visible.
Now change the field of view of your camera to 30 so that your camera has full view of the scene; you can change these settings slightly depending on what you want.
To add background textures, go to the texture randomizer section below.

### SmoothSetZero

We do not want our background to reflect light, therefore this randomizer insures that its smoothness is zero. To do this add a SmoothSetZero tag to the background cube asset by clicking add component --> SmoothSetZeroTag.
This tag can also be added to other obejcts. More info on smoothness [here](https://docs.unity3d.com/540/Documentation/Manual/StandardShaderMaterialParameterSmoothness.html).

### ForegroundObjectPlacementRandomizer

![alt text](demo_images/demo_6.PNG) 

This randomizer is for our training data object placement.
Click add folder and select your Object Prefabs folder. Put depth at -6, separation distance at 5 and placement area at X:11, Y:11. Now run the simulation and observe the results; if the objects have a scale too large or small, edit the values in the scale randomizer below. If you want less or more objects in your images or you want your objects further or closer to eachother, you can edit the separation distance, placement area and probabilities to your liking.

### ForegroundRandomObjectPlacementRandomizer

![alt text](demo_images/demo_7.PNG) 

The foreground objects have a high contrast with the background scene and are always in full view; To make your model more robust you might want to add extra random objects in the foreground that can also partly obscure your training data objects.
I added a folder with some cubes, cylinders and spheres for this purpose to this github page. However you can also find them in the perception tutorial files under background objects/prefabs.
Add this folder to this randomizer and put depth at -7 (closer to camera than training objects), separation distance at 5 and placement area at X:11, Y:11.

### TextureRandomizer

![alt text](demo_images/demo_8.PNG)  

For the random textures it is best to download a large image dataset depending on your data. For example, you can use [this indoor scene dataset](http://web.mit.edu/torralba/www/indoor.html) if you want to train object detection on objects typically located indoors. (This dataset also differentiates between different rooms so depending on your data you may only include certain rooms here) This dataset is for research purposes only.

Add a TextureRandomizerTag to your background image cube that you used in the BackgroundObjectPlacementRandomizer. Also add TextureRandomizerTags to all foreground random objects (these objects are small so the scenes will be unrecognisable on them but since they are only present for model robustness this is fine.

### HueOffsetRandomizer

This will alter the color of the random objects, add the HueOffsetRandomizerTag to the foreground random objects.

### RotationRandomizer

After adding this randomizer add the RotationRandomizerTag to all objects (except the background scene cube) for variation in rotation.

### MyLightRandomizer

Add the MyLightRandomizerTag to the light in the scene for variation on light intensity after adding this randomizer.


### ScaleRandomizer

Add ScaleRandomizertags to all training data objects in your Object Prefabs folder.
Select ranges for the scale of this randomizer depending on visual feedback from running your simulation and how big you want your objects to be (for example: 0.8 to 1.5).
You can edit this script to change the base scale to the average scale of your objects if changes the range values does not work well.

### BackgroundScaleRandomizer

in case you want different scales for your random foreground objects.
Add BackgroundScaleRandomizertags to all training data objects in your Object Prefabs folder.
Select ranges for the scale of this randomizer depending on how big you want these objects to be (for example: 0.8 to 1.5).

### SmoothnessRandomizer

Depending on the properties of your objects, you might want more or less reflections. If so choose and appropriate smoothness range. Otherwise do not add this randomizer.

![alt text](demo_images/demo_9.PNG)  

## Converting training and test data

Now you can run your simulation! First put the number of iterations to a low amount and run it to test if the images are generated as intended. You can find the images if you click on your camera and then click on "show folder" in the inspection tab under latest output folder. 

![alt text](demo_images/unity_example.PNG)  

If the images are generated as you intended, run your simulation for how many images you want. A few thousand images can already greatly improve your models.
Now copy the entire folder containing all 4 subfolders to the detection directory of this git directory.
Now run the python script unity_to_xml.py with as -x argument the name of your unity data folder (python unity_to_xml.py -x FOLDERNAME). This should create the PASCAL VOC files in the annotated folder (need to install python packages EasyDict and datasetinsights for this script to work).
You can also use online tools like Roboflow to convert your data if this does not work.

For the rest of your training data and your test data you will need real-world data. You can look for datasets online or take your own pictures. Resize all your pictures to the same size. To label your pictures you can use [LabelImg](https://github.com/tzutalin/labelImg). 


Install this package (pip3 install labelImg) and run it. Put your directory and your save directory as the same (your pictures folder), use PASCAL VOC as output and start labeling (make sure your labels are named the same as your synthetic data labels).

![alt text](demo_images/demo_10.PNG)  

Now divide your images into a test and training set. best to also use another held back test set and use your first test set as validation set to tune model parameters (use an appropriate train-val-test split, make sure there is no overlap). 
Put your training data in a new folder images/train together with all the synthetic data (images + PASCAL VOC annotations), put your test data in images/test.

[This guide](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html) from the tensorflow object detection API explains well how to do this, as well as all the following (including training the object detection model).

Now create a folder: annotations and create a label_map.pbtxt files in this folder as follows:

item {  
      &emsp;id: 1,  
      &emsp;name: "label1"  
}  
item {  
      &emsp;id: 2,  
      &emsp;name: "label2"  
}  


To then generate the tensorflow record files we then run the following script

Create train data:

 * python generate_tfrecord.py -x images/train -l annotations/label_map.pbtxt -o annotations/train.record

Create test data:

 * python generate_tfrecord.py -x images/test -l annotations/label_map.pbtxt -o annotations/test.record

Now we have our training and test data and can begin training our model!

## Training the Object detection model

[This tutorial](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html) explains very well how to train a model.
Choose a model from [the tensorflow model zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md).
For example SSD MobileNet is a relatively fast model that can obtain decent results.

Download the model and put it in a folder 'pre-trained-models'.
Copy the file 'pipeline.config' and put this file into a new folder 'models/my_model'.
Now edit this config file with the appropiate number of classes, batchsize, input- and label_map and  fine_tune_checkpoint paths (checkpoint of previously downloaded model).
As well as total number of steps, etc... . [here](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html) more information.
Make sure you use metrics_set: "coco_detection_metrics" and use_bfloat16: false if not training on a TPU.

Now we can start training:

 * python model_main_tf2.py --model_dir=models/mymodel --pipeline_config_path=models/my_model/pipeline.config

We can also start evaluation at the same time on a different terminal (this will slow down training).

 * python model_main_tf2.py --model_dir=models/my_model --pipeline_config_path=models/my_model/pipeline.config --checkpoint_dir=models/my_model

You can see the results on tensorboard:

 * tensorboard --logdir=models/my_model

![alt text](demo_images/demo_11.PNG)  

Lastly we can export the model.

 * python .\exporter_main_v2.py --input_type image_tensor --pipeline_config_path .\models\my_model\pipeline.config --trained_checkpoint_dir .\models\my_model\ --output_directory .\exported-models\exported_model_1

