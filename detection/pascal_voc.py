#! -*- coding: utf-8 -*-


import os
from PIL import Image

from utils.file_utils import create_if_not_exists, copy_file
from utils.xml_utils import create_xml_file



class PascalVoc(object):

    def __init__(self, data, out_dir, attrs):
        self._data = data
        self._out_dir = out_dir
        self._attrs = attrs

    def _create_annotation(self, image_idx, boxes):
        anno_file = os.path.join(self._out_dir, "{:06d}.xml".format(image_idx))
        attrs = dict()
        attrs['image_name'] = "{:06d}.png".format(image_idx)
        attrs['boxes'] = boxes

        img = Image.open(os.path.join(self._out_dir, "{:06d}.png".format(image_idx)))
        width, height = img.size
        attrs['width'] = str(width)
        attrs['height'] = str(height)
        for k, v in self._attrs.items():
            attrs[k] = v
        create_xml_file(anno_file, attrs)

    def make_xml_files(self, start_idx=1):


        n = 0
        for line in self._data:
            line_split = line.strip().split(" ")

            # image path
            image_path = line_split[0]

            # bounding box
            boxes = []
            for i in range(int((len(line_split) - 1) / 5)):
                category = line_split[1 + i * 5 + 0]
                x1 = line_split[1 + i * 5 + 1]
                y1 = line_split[1 + i * 5 + 2]
                x2 = line_split[1 + i * 5 + 3]
                y2 = line_split[1 + i * 5 + 4]
                boxes.append((category, x1, y1, x2, y2))

            image_idx = start_idx + n
            n += 1

            # printing progress
            if n%100 == 0:
                 print("processing: {}".format(image_path))

            # copy and rename image
            copy_file(image_path, self._out_dir, '{:06}.png'.format(image_idx))

            # create xml annotation file
            self._create_annotation(image_idx, boxes)
        return n
