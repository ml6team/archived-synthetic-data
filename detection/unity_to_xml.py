import os
import pathlib
from ipywidgets import interact, interactive, fixed, interact_manual
from PIL import Image
import numpy as np
from datasetinsights.datasets.unity_perception.exceptions import DefinitionIDError
from pascal_voc import PascalVoc
from datasetinsights.datasets.unity_perception.captures import Captures
from easydict import EasyDict as edict
import argparse



parser = argparse.ArgumentParser(
    description="Inity JSON to XML converter")
parser.add_argument("-x",
                    "--data_dir",
                    help="Path to the folder where the unity data files are stored.",
                    type=str)
args = parser.parse_args()

config = edict()

config.author = "anonymous"
config.root = "annotation"
config.folder = "VOC"
config.annotation = "PASCAL VOC"
config.segmented = "0"
config.difficult = "0"
config.truncated = "0"
config.pose = "Unspecified"
config.database = "Synth"
config.depth = "3"

#put bounding box information into a string
def make_boxstring(raw_list):
    boxstring = " "
    for obj in raw_list:
        boxstring=boxstring+obj['label_name']+" "+str(obj['x'])+ " "+str(obj['y'])+ " " + str(obj['x']+obj['width']) + " " + str(obj['y']+obj['height']) + " "
    return boxstring

#get bounding box information from unity json files
def make_string_list(dataframe):
    string_list=np.zeros(num_im, dtype=object)
    for idx in range(num_im):
        image_file= rgb_folder+"/rgb_"+str(idx+2)+".png"
        box_raw = cap_df.loc[cap_df['filename'] == image_file]["annotations"].tolist()[0][0]["values"]
        string_list[idx] =os.path.abspath(dataset_folder+"/"+image_file)+ make_boxstring(box_raw)
    return string_list
  
if __name__ == "__main__":
    
    #set dataset folder to argument
    dataset_folder = args.data_dir #os.listdir()[0]
    #rgb folder is 3rd folder in the dataset; change if not the case
    rgb_folder = os.listdir(dataset_folder)[2]
    rgb_dir = dataset_folder + '/' + rgb_folder
    num_im= len([name for name in os.listdir(rgb_dir)])


    data = Captures(dataset_folder)
    cap_df = data.captures

    box_data = make_string_list(cap_df)
    
    xml = PascalVoc(box_data, "annotated", config)
    xml.make_xml_files()

    
    

